This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# INSTALL

```javascript
npm install @types/react-router-dom
npm install react-router-dom

npm install bootstrap
```
# TODO

Creare un repository per dimostrare le funzionalità del router e la gestione del processo di autenticazione

* Creare JSON SERVER:

```javascript
{
  "login": {
    "accessToken": "efwefewfe123"
  },
  "users": [
    {
      "id": 1,
      "name": "Mario"
    }   
  ]
}
```

* Routes: `/login`, `/users`, `/home`
    * Creare una navigation bar anche con pulsante Logout

* `/login`: 
    * contiene semplice login: due campi di input e un button
    * non splittare in componenti. Non integrare troppa grafica. Semplice
    * Al submit invocare l'endpoint `login`. 
    * Creerei La chiamata la farei direttamente nel componente. Non servono classi ad hoc a meno che non si voglia sfruttare lo stesso INTERCEPTOR creato per `/users` tenendo conto che nel login non va iniettato il token
    * Dopo il login si salva il token da qualche parte (classe statica?) e magari in localstorage

* `/users`:
    * sezione banale che carica semplicemente un elenco utenti. Non serve CRUD
    * INTERCEPTOR 1: importante però che in questa GET venga iniettato il token acquisito in fase di login. Sarebbe l'ideale creare un interceptor, ovvero un semplice servizio (modulo ES6) con le chiamate GET e POST che prendono il token
    * INTERCEPTOR 2: gestire il caso in cui il token sia scaduto 401, ma visto che non si può simulare con json-server, lo simuleremo buttando giu il server e gestendo il caso 404 e default con redirect a login.  
    * GUARD 1: la view va protetta da accesso indesiderato. Si verifica se c'è il token in memoria
    * GUARD 2: far in modo che si possa accedere a questa view tramite url diretto nel caso ci sia un token in localstorage.
    
* `/logout`
    * Al click su logout si rimanda l'utente al login, si svuota token e localstorage


---

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
